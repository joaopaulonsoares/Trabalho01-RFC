#ifndef CLIENT_H
#define CLIENT_H

#define server_name "localhost"
#define server_port 8888

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <poll.h>
#include <time.h>

#include "generic.h"

int rsvFunction(char *argv[]);
int rttFunction(char *argv[]);

#endif
