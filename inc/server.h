#ifndef SERVER_H
#define SERVER_H

#define SERVER_PORT 8888

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <time.h>
#include "generic.h"


void resolveRsv(char* host_name, char* ip);
void resolveRtt(char* answer);

#endif
