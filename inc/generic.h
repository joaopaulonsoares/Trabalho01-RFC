#ifndef GENERIC_H
#define GENERIC_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct teste{
  char oper[4];
  char url[100];
};

void allocateMemory(char **ptr, int size);
int calculateSize(char* hostName, int additional);

#endif
