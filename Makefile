INCFOLDER := inc/
SRCFOLDER := src/
CC := gcc
CFLAGS :=
SRCFILES := $(wildcard src/*.c)
all: $(SRCFILES:src/%.c=%.o)
	$(CC) $(CFLAGS) client.o generic.o -o client
	$(CC) $(CFLAGS) server.o generic.o -o server
	rm -rf *.o
%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc
.PHONY: clean
clean:
	rm -rf server
	rm -rf client
