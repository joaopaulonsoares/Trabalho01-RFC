#include "server.h"
#include "generic.h"

int main(int argc,char *argv[ ]){
    // port to start the server on
  	// int SERVER_PORT = 8888;
    char answer[100];
    struct sockaddr_in si_me, si_other;
    int s, i, slen = sizeof(si_other) , recv_len;

    //create a UDP socket
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1){
      printf("Socket Error");
    }

    // zero out the structure
    memset((char *) &si_me, 0, sizeof(si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(SERVER_PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind socket to port
    if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1){
      printf("Bind Error");
    }
    char* buffer;
    allocateMemory(&buffer, 500);

    // run indefinitely
    while (1) {
      fflush(stdout);

      // read content into buffer from an incoming client
      if ((recv_len = recvfrom(s, buffer, 500, 0, (struct sockaddr *) &si_other, &slen)) == -1){
        printf("Receive Error");
      }
      buffer[recv_len] = '\0';

      char* token;
      struct teste teste_var;
      char * string;

      string = strtok (buffer," :");
      strcpy(teste_var.oper, string);
      string = strtok (NULL, " :");
      if(string != NULL){
         strcpy(teste_var.url, string);
         string = strtok (NULL, " :");
      }

      if(!strcmp(teste_var.oper,"rsv")){
        resolveRsv(teste_var.url,answer);
        if(sendto(s, answer, sizeof(answer), 0, (struct sockaddr*) &si_other, slen) == -1){
          printf("Send Error");
        }
      }else if(!strcmp(teste_var.oper,"rtt")){
        resolveRtt(answer);
        // printf("Hora Servidor: %s\n",answer);

        if(sendto(s, answer, sizeof(answer), 0, (struct sockaddr*) &si_other, slen) == -1){
          printf("Send Error");
        }
      }
    }

    free(buffer);
    return 1;
}

void resolveRsv(char* host_name, char* answer){
  struct hostent *he;
  struct in_addr **addr_list;

  if ((he = gethostbyname( host_name ) ) == NULL){
    strcpy(answer,"Rrsv: 1 Nao encontrado");
  }else {
    addr_list = (struct in_addr **) he->h_addr_list;
    strcpy(answer,"Rrsv: 0 ");
    strcat(answer, inet_ntoa(*addr_list[0]));

    //for(int i = 0; addr_list[i] != NULL; i++){
    //strcat(answer, inet_ntoa(*addr_list[i]));
    //}
  }
}

void resolveRtt(char* answer){
  time_t timer;
  struct tm* tm_info;
  char currentTime[20];

  time(&timer);
  tm_info = localtime(&timer);
  strcpy(answer, "Rrtt: ");

  strftime(currentTime, 26, "%H:%M:%S", tm_info);
  strcat(answer, currentTime);
}
