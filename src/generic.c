#include "generic.h"

void allocateMemory(char **ptr, int size) { 
    *ptr = NULL; 

    *ptr = (char*) malloc(size); 

    if(*ptr== NULL) {
        free(*ptr);      /* this line is completely redundant */
        printf("\nERROR: Memory allocation did not complete successfully!"); 
    } 
}

int calculateSize(char* hostName, int additional){
    return strlen(hostName) + additional;
}