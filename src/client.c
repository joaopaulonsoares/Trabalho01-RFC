#include "client.h"

int main(int argc, char *argv[]){

  //Server_name and server_port defined in client.h
  // Server name = localhost
  // Server port = 8888

  if (!strcmp(argv[1], "rsv") && argc==3){
      //printf("[DEBUG]Escolhido RSV\n");
      rsvFunction(argv);

  }else if (!strcmp(argv[1], "rtt")){
      //printf("[DEBUG]Escolhido RTT\n");
      rttFunction(argv);
  }

  return 0;
}

int rttFunction(char *argv[]){

    //printf("[DEBUG]Função RTT\n");

    int size = calculateSize(argv[1], 1);
    char * payload;
    allocateMemory(&payload, size);

    strcpy(payload, "rtt");
    //printf("%d\n", size);
    payload[size] = '\0';

    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;

    // Creates binary representation of server name and stores it as sin_addr
    // http://beej.us/guide/bgnet/output/html/multipage/inet_ntopman.html
    inet_pton(AF_INET, server_name, &server_address.sin_addr);

    // htons: port in network order format
    server_address.sin_port = htons(server_port);

    for (int i = 0; i < 10; i++){
        // Ppen socket
        int sock;
        if ((sock = socket(PF_INET, SOCK_DGRAM, 0)) < 0){
            printf("[DEBUG]Erro ao criar socket!\n");
            return 1;
        }

        clock_t begin, end;
        begin = clock();

        //Send data to server
        int len = sendto(sock, payload, strlen(payload), 0,
                  (struct sockaddr *)&server_address, sizeof(server_address));

        //Received echoed data back
        char answer[100];
        int received=0;
        struct pollfd fd;
        int ret=0;

        fd.fd = sock; // your socket handler
        fd.events = POLLIN;
        ret = poll(&fd, 1, 1000); // 1 second for timeout
        fflush(stdout);
        switch (ret){
        case -1:
            // Error
            printf("error");
            break;
        case 0:
            // Timeout
            printf("[Timeout]Pacote perdido ou IP do servidor inválido\n");
            break;
        default:
            recvfrom(sock, answer, 100, 0, NULL, NULL);
            close(sock);
            end = clock();
            int time_spent = (int)(end - begin);
            printf("%s Tempo RTT: %d ms\n", answer, time_spent);
            break;
        }
    }
    free(payload);
    return 0;
}

int rsvFunction(char *argv[]){
//  printf("[DEBUG]In function RSV\n");

  int size = calculateSize(argv[2], 6);
  char * payload;
  allocateMemory(&payload, size);

  //Creating payload
  strcpy(payload, "rsv: ");
  strcat(payload, argv[2]);
  payload[size-1] = '\0';

  //Configuring connection to server to send payload
  struct sockaddr_in server_address;
  memset(&server_address, 0, sizeof(server_address));
  server_address.sin_family = AF_INET;
  inet_pton(AF_INET, server_name, &server_address.sin_addr);
  server_address.sin_port = htons(server_port);

  //Open socket
  int sock;
  if ((sock = socket(PF_INET, SOCK_DGRAM, 0)) < 0){
      //printf("[DEBUG]Erro ao criar socket!\n");
      return 1;
  }

  //Configuring MAX time to server answer
  struct timeval timeout;
  timeout.tv_sec = 1;
  timeout.tv_usec = 0;
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));

  //Send package to server
  int len = sendto(sock, payload, strlen(payload), 0,
            (struct sockaddr *)&server_address, sizeof(server_address));
  free(payload);

  //Buffer corresponds to the answer received from server
  char buffer[500];
  int receive = recvfrom(sock, &buffer, 500, 0, NULL, NULL);
  if(receive == -1){
      printf("Pacote perdido ou IP do servidor inválido.\n");
  }else{
    printf("%s\n", buffer);
  }
  return 0;
}
